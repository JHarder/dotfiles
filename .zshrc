# The following lines were added by compinstall

zstyle ':completion:*' completer _complete _ignored _approximate
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %p
zstyle ':completion:*' matcher-list 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' use-compctl true
zstyle ':completion:*' verbose true
zstyle :compinstall filename '/home/jon/.zshrc'

autoload -Uz compinit
compinit
autoload -U promptinit
promptinit
# End of lines added by compinstall
# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob notify
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

alias c=clear
alias q=exit
alias ls="ls --color=auto"
LS_COLORS='di=34:fi=0:ex=32'
export LS_COLORS
alias pacup="pacaur -Syyu"
PATH=$PATH:~/.cabal/bin/
alias pacs="pacaur -Ss"
alias pacin="pacaur -S"

autoload -U colors && colors

# green user name in yellow directory (substituting /home/$USER for
# $F{color}stuff%f
# makes stuff the color given inbtween curly braces
# ~ then cyan $
PROMPT="%F{green}%n%f %F{yellow}in%f %F{blue}%~%f %F{cyan}$%f "
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
