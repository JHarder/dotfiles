setlocal omnifunc=necoghc#omnifunc

function! Ghci()
    call Repl("\"ghci " . expand("%") . "\"")
endfunction

nnoremap <leader>l :call Ghci()<cr>
