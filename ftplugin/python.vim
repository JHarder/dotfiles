function! Python()
    call Repl("\"ipython " . expand("%") . "\"")
endfunction

nnoremap <leader>l :call Python()<cr>
