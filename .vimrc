" plugins {{{1
set shell=/bin/sh
call plug#begin('~/.vim/plugged')


" org-mode require an additional plugin: vim-speeddating
" Plug 'spolu/dwm.vim'
Plug 'sjl/badwolf'
Plug 'chriskempson/tomorrow-theme'
Plug 'nathanaelkane/vim-indent-guides'
Plug 'valloric/YouCompleteMe', {'do': './install.sh'}
Plug 'majutsushi/tagbar'
Plug 'haya14busa/incsearch.vim'
Plug 'sergey-vlasov/ctrlp-hibuff'
Plug 'Shougo/vimproc.vim', {'do': 'make'}
Plug 'SirVer/ultisnips'
Plug 'amdt/vim-niji'
" Plug 'Lokaltog/powerline'
" Plug 'bling/vim-airline'
Plug 'tpope/vim-fugitive'
Plug 'ctrlpvim/ctrlp.vim'
" Plug 'dag/vim-fish'
Plug 'honza/vim-snippets'
" Plug 'jceb/vim-orgmode'
" Plug 'katono/rogue.vim'
Plug 'morhetz/gruvbox'
Plug 'ntpeters/vim-better-whitespace'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/syntastic'
Plug 'sheerun/vim-polyglot'
" Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-surround'
" Plug 'travitch/hasksyn'
" Plug 'altercation/vim-colors-solarized'
Plug 'davidhalter/jedi-vim'

call plug#end()
"}}}1

" make vim modern {{{1
let mapleader=" "

if has("gui_running")
    set background=light
else
    set background=dark
endif

set background=dark

colorscheme delek
" colorscheme Tomorrow-Night-Bright
" colorscheme elflord
" colorscheme solarized
filetype plugin indent on
syntax on
set omnifunc=syntaxcomplete#Complete
set noerrorbells
set t_vb=
"}}}1

" plugin configuration {{{1

" tagbar config {{{2
nnoremap <leader>= :TagbarToggle<cr>
" }}}2

" vim-niji (rainbow parenthesis) configuration {{{2
let g:niji_matching_filetypes = ['haskell', 'opencl', 'lisp', 'scheme', 'clojure']
"let g:niji_dark_colours = [['brown', 'RoalBlue3'],
                        "\  ['Darkblue', 'SeaGreen3'],
                        "\  ['darkgrey', 'DarkOrchid3'],
                        "\  ['darkgreen', 'firebrick3'],
                        "\  ['darkcyan', 'RoyalBlue3']]
let g:niji_dark_colours = [
    \ [ '81', '#5fd7ff'],
    \ [ '99', '#875fff'],
    \ [ '1',  '#dc322f'],
    \ [ '76', '#5fd700'],
    \ [ '3',  '#b58900'],
    \ [ '2',  '#859900'],
    \ [ '6',  '#2aa198'],
    \ [ '4',  '#268bd2'],
    \ ]
"}}}2

" ctrlp configuration {{{2
if executable('ag')
    set grepprg=ag\ --nogroup\ --nocolor
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif
nnoremap <leader>f :CtrlPMRU<cr>
nnoremap <leader>b :CtrlPBuffer<cr>
"}}}2

" ctrlp-hibuff {{{2
nnoremap <C-b> :CtrlPHiBuff<cr>
"}}}2

" incsearch {{{2
map / <Plug>(incsearch-forward)
map ? <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
" }}}2

" airline configuration {{{2
" speed up initialization
let g:airline#extensions#tagbar#enabled = 0
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=0
let g:airline#extensions#syntastic#enabled = 1
let g:airline#extensions#tabline#enabled =  0
" let g:airline_theme="powerlineish"
"}}}2

" syntastic configuration {{{2
let g:syntastic_cpp_compiler="g++"
let g:syntastic_cpp_compiler_options=" -std=c++1y"
let g:syntastic_python_python_exec = 'python'
" let g:syntastic_cpp_compiler_options=" -std=c++11"
"}}}2

" ultisnips configuration {{{2
let g:UltiSnipsExpandTrigger="<c-l>"
let g:UltiSnipsJumpForwardTrigger="<c-j>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"
let g:UltiSnipsEditSplit="horizontal"
"}}}2
"}}}1

" status bar config {{{1
hi StatusLine ctermbg=040 ctermfg=232

set statusline=%t       " tail of the filename
set statusline+=\ [%{strlen(&fenc)?&fenc:'none'}, " fine encoding
set statusline+=%{&ff}] " file format
set statusline+=%h      " help file flag
set statusline+=%m      " modified flag
set statusline+=%r      " read only flag
set statusline+=%y      " filetype
set statusline+=%=      " left/right sepparator
set statusline+=size:\ %{FileSize()},
set statusline+=\ %c,     " cursor column
set statusline+=%l/%L   " cursur line/total lines
set statusline+=\ %P    " percent through file

function! FileSize()
    let bytes = getfsize(expand("%:p"))
    if bytes <= 0
        return ""
    endif
    if bytes < 1024
        return bytes
    else
        return (bytes / 1024) . "K"
    endif
endfunction
" }}}1

" settings {{{1
" set listchars=eol:¬,tab:→→,extends:>,precedes:<
set listchars=eol:¬,tab:>-,extends:>,precedes:<
set list
set mouse=a
set hidden
set guioptions=""
" make the status bar always visible
set laststatus=2
set backspace=indent,eol,start
" set cursorline
set nonu
set nornu
set wildmenu
set wildmode=full
set wildignore=*.swp,*.bak,*.pyc,*.so,*.hi,*.o,*.zip
set ttimeoutlen=50
set incsearch
set hlsearch
set showmatch
set ignorecase
" Auto read when a file is changed from the outside
set autoread
" make tabs behave nicely
set ts=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab
set foldmethod=marker
"}}}1

" mappings {{{1
" make moving splits easier
nnoremap <leader> <C-w>
nnoremap <leader><leader> <C-w><C-w>
nnoremap <leader>e :e ~/.vimrc<cr>
nnoremap <leader>w :w<cr>
nnoremap <silent> <leader>s :call Shell()<cr>
nnoremap <leader>n :bn<cr>
nnoremap <leader>d :bd<cr>
nnoremap <leader>q :q<cr>
" nnoremap <leader>c :call ChangeToBufferDir()<cr>
nnoremap <leader>h :noh<cr>

" make Y behave like D and C
nnoremap Y y$

" make macros easier to record/execute
nnoremap Q @q
vnoremap Q :norm @q<cr>

" n/vim like a real man (or woman)
map <up>    :echom "use k"<cr>
map <left>  :echom "use h"<cr>
map <right> :echom "use l"<cr>
map <down>  :echom "use j"<cr>
imap <up>    <esc>:echom "use k"<cr>
imap <left>  <esc>:echom "use h"<cr>
imap <right> <esc>:echom "use l"<cr>
imap <down>  <esc>:echom "use j"<cr>
"}}}1

" Commands {{{1
command! W exec 'w !sudo tee % > /dev/null' | e!
command! So exec 'so ~/.vimrc'
"}}}1

" autocommands {{{1
"}}}1

" custon functions {{{1
function! Shell()
    silent :!tmux split -v
    silent :!tmux select-layout main-horizontal
endfunction

function! Repl(repl)
    silent exec ":!tmux split-window -h " . "\"" . a:repl "\""
endfunction

function! ChangeToBufferDir()
    cd %:h
    echom "moved to" getcwd()
endfunction
"}}}1
