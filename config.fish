# use hash tags for comments
set fish_greeting ""

set -x PAGER less

function fish_prompt -d "Write out the prompt"
	set_color green --bold
	echo -n '╭─'
    echo -n "("
	set_color --bold blue
	echo -n (whoami)
	set_color green
    echo -n ")─["
	set_color blue --bold
	echo -n (prompt_pwd)
    set_color green --bold
	echo ']'
	set_color green --bold
	echo -n '╰─'
	echo -n '>'
	set_color green
end

alias q exit
alias c clear
alias claer clear
alias ls "ls --indicator-style=none --color=auto"
alias tmux "tmux -2"
alias :q exit
alias l "ls -F --color=auto"
alias w3m "w3m duckduckgo.com"

alias pacup "sudo pacman -Syyu"
alias pacin "sudo pacman -S"
alias pacs "pacman -Q"

set PATH ~/bin $PATH

set -U AWESOME ~/.config/awesome/rc.lua
