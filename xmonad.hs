import XMonad

-- import XMonad.Actions.Volume
import XMonad.Actions.CycleWS

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.SetWMName

import XMonad.Layout.Spacing hiding (SmartSpacing)
import XMonad.Layout.NoBorders
import XMonad.Layout.Grid
import XMonad.Layout.Tabbed

import XMonad.Util.EZConfig
import XMonad.Util.Scratchpad

import Graphics.X11.ExtraTypes.XF86

type Program = String

myTerminal :: Program
myTerminal = "urxvtc"

tmux :: Program
tmux = myTerminal ++ " -e tmux"

myWorkspaces :: [String]
myWorkspaces = ["1:main", "2:code", "3:music", "4:misc", "5", "6", "7", "8", "9"]

dmenu :: Program
dmenu = "dmenu_run -fn '-*-ohsnap-medium-*-*-*-14-*-*-*-*-*-iso8859-*' -nb '" ++ myNormalBorderColor ++ "' -nf '"  ++ myFocusedBorderColor
    ++ "' -sb '" ++ myFocusedBorderColor ++ "' -sf '" ++ myNormalBorderColor ++ "'"

myBrowser, myEditor, myMusic, myRadio, myLock, myBar, myInternet, myPDF, myUpdater, reload, configure :: Program
-- myBrowser = "firefox"
-- myBrowser = "google-chrome-stable"
myBrowser = "chromium"

myEditor = myTerminal ++ " -e nvim"

myMusic = myTerminal ++ " -e ncmpcpp"

myRadio = myTerminal ++ " -e pianobar"

myLock = "~/bin/lock"

myBar = "xmobar"

myInternet = myTerminal ++ " -e wicd-curses"

myPDF = "zathura"

myUpdater = myTerminal ++ " -e pacaur -Syyu"

reload = "xmonad --recompile; xmonad --restart"

configure = myEditor ++ " ~/.xmonad/xmonad.hs"

myNormalBorderColor :: String
myNormalBorderColor = "#101010"
myFocusedBorderColor :: String
myFocusedBorderColor = "#b58900"

myModMask :: KeyMask
myModMask = mod4Mask

data ColorTheme = ColorTheme { bg :: String, fg :: String }

solarized :: ColorTheme
solarized = ColorTheme { bg = "#002b36", fg = "#b58900" }

gruvbox :: ColorTheme
gruvbox = ColorTheme { bg = "#121212", fg = "#c59910" }

main :: IO ()
main = xmonad =<< statusBar myBar myPP toggleStrutsKey myConfig

myPP :: PP
myPP = xmobarPP { ppCurrent = xmobarColor (bg gruvbox)  (fg gruvbox)  -- . wrap "[" "]"
                , ppHidden = xmobarColor (fg gruvbox) ""
                , ppLayout = const ""
                , ppTitle = xmobarColor "#5555ff" ""
                , ppSep = "  >>=  " }

myManageHook :: ManageHook
myManageHook = scratchpadManageHookDefault <+> composeAll
    [ isFullscreen --> doFullFloat
    , className =? "Gimp"  --> doFloat
    , className =? "gimp"  --> doFloat
    , title =? "Mednagui" --> doFloat
    , title =? "Pandora Radio" --> doShift "3:music"
    , title =? "Game 0.1" --> doFloat
    , title =? "Punch Everything v.0.01" --> doFloat
    , title =? "Steam - Self Updater" --> doFloat
    , title =? "Steam - Update News" --> doFloat
    ]

toggleStrutsKey :: XConfig t -> (KeyMask, KeySym)
toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)


myConfig = defaultConfig
    { terminal          =   myTerminal
    , modMask = myModMask
    , borderWidth       =   2
    , manageHook        =   manageDocks <+> myManageHook <+> manageHook defaultConfig
    , workspaces        =   myWorkspaces
    , normalBorderColor =   myNormalBorderColor
    , focusedBorderColor=   myFocusedBorderColor
    , layoutHook        =   avoidStruts $ smartBorders myLayout
    , startupHook       =   setWMName "LG3D"
    } `additionalKeys` myKeys

myLayout = tiled ||| Grid ||| Mirror tiled ||| Full ||| simpleTabbed
    where
        tiled = smartSpacing 0 $ Tall nmaster delta ratio
        nmaster = 1
        ratio = 1/2
        delta = 3/100

modmask :: KeyMask
modmask = myModMask

fullKeyBind :: KeyMask -> KeySym -> Program -> ((KeyMask, KeySym), X ())
fullKeyBind mask key app = ((mask, key), spawn app)

-- button to bind, program to bind it to
keybind :: KeySym -> Program -> ((KeyMask, KeySym), X ())
keybind = fullKeyBind modmask

myKeys :: [((KeyMask, KeySym), X ())]
myKeys = [-- keybind xK_c "google-chrome-stable"
         --,keybind xK_c "chromium"
          keybind xK_c "chromium"
         ,keybind xK_d "dwarffortress-ih"
         -- ,keybind xK_e myEditor
         ,keybind xK_e "emacs"
         ,keybind xK_f "firefox"
         ,keybind xK_g (myTerminal ++ " -e ghci")
         ,keybind xK_i myInternet
         ,keybind xK_m myMusic
         ,keybind xK_p dmenu
         ,keybind xK_o "opera"
         ,keybind xK_q myLock
         ,keybind xK_r reload
         ,keybind xK_u myUpdater
         ,keybind xK_v "gvim"
         ,keybind xK_w myBrowser
         ,keybind xK_z myPDF
         ,(( modmask .|. shiftMask, xK_e), spawn configure)
         ,(( modmask .|. shiftMask, xK_Return), spawn tmux)
         ,(( modmask, xK_Return), spawn myTerminal)
         ,fullKeyBind (modmask .|. shiftMask) xK_p myRadio
         -- audio configuration
         ,(( modmask .|. controlMask, xK_p), spawn "echo -n ' ' > ~/.config/pianobar/ctl")
         ,(( modmask .|. controlMask, xK_n), spawn "echo -n 'n' > ~/.config/pianobar/ctl")
         ,(( modmask .|. controlMask, xK_l), shiftToNext >> nextWS)
         ,(( modmask .|. controlMask, xK_h), shiftToPrev >> prevWS)
         -- ,(( 0, xF86XK_AudioMute), toggleMute >> return ())
         -- ,(( 0, xF86XK_AudioLowerVolume), lowerVolume 2 >> return ())
         -- ,(( 0, xF86XK_AudioRaiseVolume), raiseVolume 2 >> return ())
         ,(( 0, xF86XK_AudioPlay), spawn "ncmpcpp toggle")
         ,(( 0, xF86XK_AudioNext), spawn "ncmpcpp next")
         ,(( 0, xF86XK_AudioPrev), spawn "ncmpcpp prev")
         ,(( 0, xF86XK_AudioStop), spawn "ncmpcpp stop")
         -- scratchpad actions
         ,((modmask, xK_minus), scratchpadSpawnActionTerminal myTerminal)
         ]
