;;; .emacs -- My emacs configuration file
;;; Commentary: I'm by no means familiar with elisp...but I try

;;; Code:

;; t for on, nil for off
;; (defvar vi-emu nil)
(setq vi-emu t)

(fset 'yes-or-no-p 'y-or-n-p)
(scroll-bar-mode 0)
(tool-bar-mode 0)
(menu-bar-mode 0)
(setq initial-scratch-message "")
(setq inhibit-startup-message t)
(setq visible-bell nil)
(define-key global-map (kbd "RET") 'newline-and-indent)

(defalias 'eb 'eval-buffer)

;; smooth scrolling
(setq mouse-wheel-scroll-amount '(1 ((shift) . 1))) ;; one line at a time
(setq mouse-wheel-progressive-speed nil) ;; dont accelerate scrolling
(setq mouse-wheel-follow-mouse 't) ;; scroll window under mouse
(setq scroll-step 1) ;; keyboard scroll one line at a time
(show-paren-mode 1)
(setq show-paren-style 'parenthesis)
(display-battery-mode 1)

(setq backup-directory-alist `(("." . "~/.saves")))

(blink-cursor-mode 1)

(setq initial-major-mode 'org-mode)

(let ((font "Inconsolata for Powerline-15"))
  (set-default-font font nil t)
  (set-fontset-font t '(8500 . 8800) font))

;; make tabs behave sanely
(setq-default tab-width 4
              indent-tabs-mode nil
              c-basic-offset 4)
(setq indent-line-function 'insert-tab)

(setq package-archives '(("org" . "http://orgmode.org/elpa/")
                         ("gnu" . "http://elpa.gnu.org/packages/")
                         ("marmalade" . "http://marmalade-repo.org/packages/")
                         ("elpy" . "http://joregenschaefer.github.io/packages/")
                         ("melpa" . "http://melpa.milkbox.net/packages/")
                         ("SC" . "http://joseito.republika.pl/sunrise-commander/")))
(require 'package)
(package-initialize)

(autoload 'enable-paredit-mode "paredit" "Turn on pseudo-structural editing of Lisp code." t)
(add-hook 'emacs-lisp-mode-hook #'enable-paredit-mode)
;; (add-hook 'ielm-mode-hook       #'enable-paredit-mode)

(when (require 'multi-term nil t)
  (global-set-key (kbd "<C-right>") 'multi-term-next)
  (global-set-key (kbd "<C-left>") 'multi-term-prev)
  (setq multi-term-buffer-name "shell"
        multi-term-program "/usr/bin/zsh"))

(require 'color-theme)
(eval-after-load "color-theme"
  '(progn
     (color-theme-initialize)
     (color-theme-euphoria)))
;; (load-theme 'gruvbox t)
;; (load-theme 'solarized-dark t)
;; (require 'autopair)
;; (autopair-global-mode)

(add-to-list 'custom-theme-load-path "~/.emacs.d/themes/")

;; Rainbow delimeters to easier recognize parentheses, brackets etc.
(require 'rainbow-delimiters)
(add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
(add-hook 'prog-mode-hook '(lambda ()
                             (setq show-trailing-whitespace t)))

(require 'ido)
(require 'flycheck)
(setq flycheck-clang-language-standard "-std=c++1y")
(add-hook 'after-init-hook #'global-flycheck-mode)
(add-hook 'after-init-hook #'flyspell-mode)
(ido-mode t)

(setq eshell-banner-message "")

(defun eshell/edit (file)
  (interactive)
  (find-file file))

(defun eshell/e (file)
    (interactive)
    (eshell/edit file))

(defun eshell/clear ()
  (interactive)
  (let ((inhibit-read-only t))
    (erase-buffer)))

(defun eshell/c ()
  (eshell/clear))

(defun eshell/q ()
  (eshell/exit))

(defun eshell/pacup ()
  (interactive)
  (eshell-command "sudo pacaur -Syyu"))

(defun eshell/gs ()
  (interactive)
  (eshell-command "git status"))

(defun eshell/pacs (package)
  (interactive)
  (eshell-command (concat "pacaur -S " package)))

(defun Vex ()
  (interactive)
  (split-window-right)
  (eshell))

(defun Sex ()
  (interactive)
  (split-window-below)
  (eshell))

(defun Ex ()
  (interactive)
  (eshell))

(defun pl ()
  (interactive)
  (package-list-packages))

(setq eshell-prompt-function
    (lambda ()
        (concat (user-login-name) " at " (eshell/pwd) " $ ")))

(setq eshell-hightlight-prompt nil)

;;=========================================

;; haskell stuff
(add-hook 'haskell-mode-hook 'haskell-indentation-mode)
(add-hook 'haskell-mode-hook 'inf-haskell-mode)

;;===================== turn emacs into vim ===========================
(require 'evil)
(require 'surround)
(require 'evil-leader)
;;================ evil leader config stuff

;; make jk exit insert mode (mostly for terminal emacs)
(if window-system
    (setq key-chord-two-keys-delay 0.5)
    (key-chord-define evil-insert-state-map "jk" 'evil-normal-state)
    (key-chord-mode 1))

(evil-leader/set-key
  "f" 'ido-find-file

  "e" (lambda ()
    (interactive)
    ;; (evil-window-vsplit)
    (find-file "~/.emacs"))
  "s" (lambda ()      ;; if in a vty, just use eshell for functionality and readibility
        (interactive) ;; otherwise (some for of gui) just use an actual shell
        (if window-system
            (multi-term)
            (eshell))) ;; (lambda ()
        ;; (interactive)
        ;; (evil-window-vsplit)
        ;; (evil-window-move-far-right)
        ;; (shrink-window-horizontally 15)
        ;; (eshell))
  "b" 'switch-to-buffer
  "k" (lambda ()
        (interactive)
        (kill-buffer-and-window))
  "v" 'Vex
  "m" 'magit-status
  "c" 'delete-window
  "o" 'delete-other-windows
  "w" 'basic-save-buffer
  "q" 'evil-quit
  "l" (lambda ()
        (interactive)
        (inferior-haskell-load-file)
        (other-window -1)
        (evil-window-move-far-right))
  "<SPC>" 'next-buffer
  "d" 'evil-delete-buffer
  "n" 'evil-next-buffer)
(evil-leader/set-leader "<SPC>")
;; (global-evil-leader-mode)
;; restor vims scroll up bind which emacs normally gobbles as 'universal-command

;;=========================================
(when vi-emu
    (progn
      (define-key global-map (kbd "C-u") 'evil-scroll-up)
      (global-evil-leader-mode)
      (evil-mode 1)
      ;; (global-evil-tabs-mode t)
      (global-surround-mode 1)))
;; this must be after global evil leader
(global-surround-mode 1)

(defun switch-to-last-buffer ()
  (interactive)
  (switch-to-buffer nil))

(global-set-key (kbd "C-<backspace>") 'switch-to-last-buffer)

;;=====================================================================
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(custom-safe-themes (quote ("454dc6f3a1e9e062f34c0f988bcef5d898146edc5df4aa666bf5c30bed2ada2e" default)))
 '(server-mode t))
(put 'erase-buffer 'disabled nil)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
