" plugins {{{1
set shell=/bin/sh
call plug#begin('~/.nvim/plugged')

Plug 'idris-hackers/idris-vim'
Plug 'freeo/vim-kalisi'
Plug 'haya14busa/incsearch.vim'
Plug 'sergey-vlasov/ctrlp-hibuff'
Plug 'terryma/vim-multiple-cursors'
Plug 'Shougo/neocomplcache'
Plug 'Shougo/neosnippet-snippets'
Plug 'Shougo/neosnippet.vim'
Plug 'Shougo/vimproc.vim', {'do': 'make'}
Plug 'amdt/vim-niji'
Plug 'bling/vim-airline'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'dag/vim-fish'
Plug 'godlygeek/tabular'
Plug 'morhetz/gruvbox'
Plug 'ntpeters/vim-better-whitespace'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/syntastic'
Plug 'sheerun/vim-polyglot'
Plug 'tpope/vim-surround'
Plug 'travitch/hasksyn'

call plug#end()
"}}}1

" make nvim modern {{{1
let mapleader=" "
set background=dark
" colorscheme gruvbox
colorscheme solarized
filetype plugin indent on
syntax on
"}}}1

" plugin configuration {{{1

" incsearch {{{2
map / <Plug>(incsearch-forward)
map ? <Plug>(incsearch-backward)
map g/ <Plug>(incsearch-stay)
" }}}2

" niji configuration {{{2
let g:niji_matching_filetypes = ['haskell','opencl','lisp','scheme','clojure']

let g:niji_dark_colours = [
    \ [ '81', '#5fd7ff'],
    \ [ '99', '#875fff'],
    \ [ '1',  '#dc322f'],
    \ [ '76', '#5fd700'],
    \ [ '3',  '#b58900'],
    \ [ '2',  '#859900'],
    \ [ '6',  '#2aa198'],
    \ [ '4',  '#268bd2'],
    \ ]
"}}}2

" ctrlp configuration {{{2
if executable('ag')
    set grepprg=ag\ --nogroup\ --nocolor
    let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
endif
nnoremap <leader>f :CtrlPMRU<cr>
nnoremap <leader>b :CtrlPBuffer<cr>
"}}}2

" ctrlp-hibuff {{{2
nnoremap <C-b> :CtrlPHiBuff<cr>
"}}}2

" neocomplache configuration {{{2
let g:neocomplcache_enable_at_startup = 1
let g:neocomplcache_enable_smart_case = 1
let g:neocomplcache_min_syntax_length = 3
let g:neocomplcache_lock_buffer_name_pattern = '\*ku\*'
" omnicompletion
autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
"}}}2

" airline configuration {{{2
let g:airline_powerline_fonts=1
let g:airline#extensions#tabline#enabled=0
" let g:airline_theme="powerlineish"
"}}}2

" syntastic configuration {{{2
let g:syntastic_cpp_compiler="g++"
let g:syntastic_cpp_compiler_options=" -std=c++11"
"}}}2

" neosnippet configuration {{{2
imap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: pumvisible() ? "\<C-n>" : "\<TAB>"
smap <expr><TAB> neosnippet#expandable_or_jumpable() ?
\ "\<Plug>(neosnippet_expand_or_jump)"
\: "\<TAB>"
"}}}2

"}}}1

" settings {{{1
set hidden
set guioptions=""
set noshowmode
set laststatus=2
set backspace=indent,eol,start
" set cursorline
set nonu
set nornu
set mouse=a
set wildmenu
set wildmode=full
set wildignore=*.swp,*.bak,*.pyc,*.so,*.hi,*.o,*.zip
set ttimeoutlen=50
set incsearch
set hlsearch
set showmatch
set ignorecase
" Auto read when a file is changed from the outside
set autoread
" make tabs behave nicely
set ts=4
set shiftwidth=4
set softtabstop=4
set smarttab
set expandtab
set foldmethod=marker
"}}}1

" autocommands {{{1
" recompile any latex file asyncronously when buffer is saved
au! BufWritePost *.tex :call jobstart("latex","pdflatex",[expand('%')])
"}}}1

" mappings {{{1
" leader mappings
nnoremap <leader>h :noh<cr>
nnoremap <leader>e :e ~/.nvimrc<cr>
nnoremap <leader>w :w<cr>
nnoremap <silent> <leader>s :call Shell()<cr>
nnoremap <leader>n :bn<cr>
nnoremap <leader>d :bd<cr>
nnoremap <leader>q :q<cr>
nnoremap <leader>c :call ChangeToBufferDir()<cr>

" make substitutions easier
nnoremap gs :%s/
" nnoremap <leader>p :call jobstart("latex", "pdflatex", [expand('%')])<cr>

" async make
function! JobHandle()
    if len(v:job_data) > 2
        caddexpr v:job_data[2]
    endif
endfunction
autocmd! JobActivity foo call JobHandle()

nnoremap <leader>mc :call setqflist([])<cr>:call jobstart('foo','make',['clean'])<cr>
nnoremap <leader>mk :call setqflist([])<cr>:call jobstart('foo','make')<cr>
" nnoremap <leader>h :noh<cr> "goodnight sweet prince

" split navigation
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" make Y behave like D and C
nnoremap Y y$
" smart indent when entering insert mode with i on empty lines
nnoremap <expr> i IndentWithI()
" make macros easier to record/execute
nnoremap Q @q
vnoremap Q :norm @q<cr>
" n/vim like a real man (or woman)
map <up>    :echom "use k"<cr>
map <left>  :echom "use h"<cr>
map <right> :echom "use l"<cr>
map <down>  :echom "use j"<cr>
imap <up>    <esc>:echom "use k"<cr>
imap <left>  <esc>:echom "use h"<cr>
imap <right> <esc>:echom "use l"<cr>
imap <down>  <esc>:echom "use j"<cr>
vmap <up>    <esc>:echom "use k"<cr>
vmap <left>  <esc>:echom "use h"<cr>
vmap <right> <esc>:echom "use l"<cr>
vmap <down>  <esc>:echom "use j"<cr>

vnoremap < <gv
vnoremap > >gv

"}}}1

" Commands {{{1
command! W exec 'w !sudo tee % > /dev/null' | e!
command! So exec 'so ~/.nvimrc'
"}}}1

" custon functions {{{1
function! IndentWithI()
    if len(getline('.')) == 0
        return "\"_ddO"
    else
        return "i"
    endif
endfunction

function! Shell()
    silent :!tmux split -v
    silent :!tmux select-layout main-vertical
endfunction

function! ChangeToBufferDir()
    cd %:h
    echom "moved to" getcwd()
endfunction

"
" function! Pdflatex()
"     call jobstart("latex", "pdflatex", [expand('%')])
" endfunction
"}}}1
